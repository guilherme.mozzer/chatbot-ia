﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.IO;
using System.Linq;
using System.Data;
using System.Text;

namespace camadadedados
{
    class dal
    {
        private static string servidor = "localhost";
        private static string banco = "chatbot";
        private static string user = "root";
        private static string password = "";

        private string string_conexao = "Server=" + servidor +
                                        ";database=" + banco +
                                        ";Uid=" + user +
                                        ";Pwd=" + password;
        public MySqlConnection conexao;

        public void conectado()
        {
            conexao = new MySqlConnection(string_conexao);
            conexao.Open();
        }

        public void ExecutarComandoSQL(string sql)
        {
            conectado();

            MySqlCommand comando = new MySqlCommand(sql, conexao);
            comando.ExecuteNonQuery();
        }

        public DataTable DadosDataTable(string sql)
        {
            conectado();
            DataTable dados = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sql, conexao);
            da.Fill(dados);
            conexao.Close();
            return dados;
        }
    }
}