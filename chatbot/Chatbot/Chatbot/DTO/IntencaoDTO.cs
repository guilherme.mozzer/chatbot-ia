﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camadadeapresentacao
{
    class IntencaoDTO
    {
        private int idintencao;
        private string termo;
        public int Idintencao { get => idintencao; set => idintencao = value; }
        public string Termo { get => termo; set => termo = value; }
    }
}
