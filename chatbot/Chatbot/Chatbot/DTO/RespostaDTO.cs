﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camadadeapresentacao
{
    class RespostaDTO
    {
        private int idresposta;
        private string resposta;

        public int Idresposta { get => idresposta; set => idresposta = value; }
        public string Resposta { get => resposta; set => resposta = value; }
    }
}
