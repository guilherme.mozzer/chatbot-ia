﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camadadeapresentacao
{
    class PalavraDTO
    {
        private int idpalavra;
        private string palavra;

        public int Idpalavra { get => idpalavra; set => idpalavra = value; }
        public string Palavra { get => palavra; set => palavra = value; }
    }
}
