﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using camadadeapresentacao;
using camadadenegocios;
using System.Data;
namespace Chatbot
{
    public partial class FormChat : Form
    {
        public FormChat()
        {
            InitializeComponent();

            #region MensagemInicialDoBot
            DateTime now = DateTime.UtcNow;
            if (now.Hour >= 06 && now.Hour < 12)
                //conversa.Text.Insert(conversa.Text.Length, "Ed: Bom dia! O que posso ajudar?");
                Chat.Items.Add(bot + "Bom dia! O que posso ajudar?");
            else if (now.Hour >= 12 && now.Hour < 18)
                Chat.Items.Add(bot + "Boa Tarde! O que posso ajudar?");
            else
                Chat.Items.Add(bot + "Boa Noite! O que posso ajudar?");
            #endregion
        }

        IntencaoDTO DTOi = new IntencaoDTO();
        IntencaoBLL BLLi = new IntencaoBLL();
        PalavraDTO DTOp = new PalavraDTO();
        PalavraBLL BLLp = new PalavraBLL();
        RespostaDTO DTOr = new RespostaDTO();
        RespostaBLL BLLr = new RespostaBLL();

        string bot = "Ed:\t", eu = "Você:\t";
        string[] pfrase;
        List<string> RespBot = new List<string>();
        List<int> VerifcaIntecoes = new List<int>();
        List<int> Intecoes = new List<int>();

        public static string removerAcentos(string texto)
        {
            string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛÙüúûùÇç";
            string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUUuuuuCc";

            for (int i = 0; i < comAcentos.Length; i++)
            {
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }
            return texto;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPergunta.Text))
            {
                lbBot.Visible = true;

                Chat.Items.Add("");
                Chat.Items.Add(eu + txtPergunta.Text);

                pfrase = removerAcentos(txtPergunta.Text.ToUpper()).Split(' ');

                int i = 0;
                while (i < pfrase.Length)
                {
                    DTOp.Palavra = pfrase[i];


                    DataTable dt = BLLp.listarPalavraExiste(DTOp);


                    int j = 0;
                    while (j < dt.Rows.Count)
                    {

                        DTOp.Idpalavra = Convert.ToInt32(dt.Rows[j]["idpalavra"].ToString());

                        DataTable dti = BLLp.listarIntencoesPalavra(DTOp);

                        if (dti.Rows.Count > 0)
                        {
                            int k = 0;
                            while (k < dti.Rows.Count)
                            {
                                RespBot.Add(dti.Rows[k]["termos"].ToString());
                                k++;
                            }
                        }
                        j++;
                    }
                    i++;
                }
                

                var dups = RespBot.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key).ToList();

                if (dups.Count > 0)
                {
                    if (dups.Count == 1)
                    {
                        DTOi.Termo = dups[0].ToString();

                        DataTable dt = BLLi.listarRespostaIntencao(DTOi);

                        if (dt.Rows.Count > 0)
                        {
                            Chat.Items.Add("");
                            Chat.Items.Add(bot + dt.Rows[0]["resposta"].ToString());
                        }
                    }
                    else
                    {

                        Chat.Items.Add("");
                        Chat.Items.Add(bot + " Selecione o que deseja!");

                        foreach (var r in dups)
                        {
                            Chat.Items.Add("\t> " + r);
                        }
                    }
                }
                else
                {
                    if (RespBot.Count > 1)
                    {
                        Chat.Items.Add("");
                        Chat.Items.Add(bot + " Selecione o que deseja!");

                        foreach (var r in RespBot)
                        {
                            Chat.Items.Add("\t> " + r);
                        }

                    }
                    else if (RespBot.Count == 1)
                    {

                        DTOi.Termo = RespBot[0];

                        DataTable dt = BLLi.listarRespostaIntencao(DTOi);

                        if (dt.Rows.Count > 0)
                        {
                            Chat.Items.Add("");
                            Chat.Items.Add(bot + dt.Rows[0]["resposta"].ToString());
                        }
                    }
                    else
                    {
                        Chat.Items.Add("");
                        Chat.Items.Add(bot + "Não entendi o que você disse! ");
                        Chat.Items.Add("\tVerifique os erros de ortografia ou reformule a frase e tente novamente!");
                    }
                }

                dups.Clear();
                RespBot.Clear();
                lbBot.Visible = false;
                txtPergunta.Text = "";
                txtPergunta.Focus();
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Chat.Items.Clear();
        }

        private void FormChat_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                            btnEnviar_Click(sender, e);
                    }
                    break;

                case Keys.Escape:
                    {
                            btnFechar_Click(sender, e);
                    }
                    break;
                case Keys.F3:
                    {
                            btnLimpar_Click(sender, e);
                    }
                    break;
            }
        }

        private void txtPergunta_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        btnEnviar_Click(sender, e);
                    }
                    break;

                case Keys.Escape:
                    {
                        btnFechar_Click(sender, e);
                    }
                    break;
                case Keys.F3:
                    {
                        btnLimpar_Click(sender, e);
                    }
                    break;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
