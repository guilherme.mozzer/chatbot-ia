﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using camadadedados;
using camadadeapresentacao;
using System.Data;

namespace camadadenegocios
{
    class IntencaoBLL
    {
        dal objDAL = new dal();

        public DataTable inserir(IntencaoDTO i)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("INSERT INTO Intencao (termos) VALUES ('{0}'); select last_insert_id() as id;", i.Termo);
            data = objDAL.DadosDataTable(sql);
            return data;
        }

        public void alterar(IntencaoDTO i)
        {
            objDAL.conectado();
            string sql = String.Format("UPDATE Intencao set termos = '{0}' where idintencao = {1}", i.Termo, i.Idintencao);
            objDAL.ExecutarComandoSQL(sql);
        }

        public void excluir(IntencaoDTO i)
        {
            objDAL.conectado();
            string sql = String.Format("DELETE FROM Intencao where idintencao = {0}", i.Idintencao);
            objDAL.ExecutarComandoSQL(sql);
        }

        public DataTable listarRespostaIntencao(IntencaoDTO i)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("SELECT i.termos, r.resposta FROM intencaoresposta as ir INNER JOIN  intencao as i on ir.intencao = i.idintencao inner join resposta as r on ir.resposta = r.idresposta WHERE i.termos = '{0}'; ", i.Termo);
            data = objDAL.DadosDataTable(sql);
            return data;
        }
    }
}