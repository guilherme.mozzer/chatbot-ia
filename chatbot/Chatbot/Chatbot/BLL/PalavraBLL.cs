﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using camadadedados;
using camadadeapresentacao;
using System.Data;

namespace camadadenegocios
{
    class PalavraBLL
    {
        dal objDAL = new dal();

        public DataTable inserir(PalavraDTO p)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("INSERT INTO PalavrasChave (descricao) VALUES ('{0}'); select last_insert_id() as id;", p.Palavra);
            data = objDAL.DadosDataTable(sql);
            return data;
        }

        public void alterar(PalavraDTO p)
        {
            objDAL.conectado();
            string sql = String.Format("UPDATE PalavrasChave set descricao = '{0}' where idpalavra = {1}", p.Palavra, p.Idpalavra);
            objDAL.ExecutarComandoSQL(sql);
        }

        public void excluir(PalavraDTO p)
        {
            objDAL.conectado();
            string sql = String.Format("DELETE FROM PalavrasChave where idpalavra = {0}", p.Idpalavra);
            objDAL.ExecutarComandoSQL(sql);
        }

        public DataTable listarPalavraExiste(PalavraDTO p)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("SELECT idpalavra, descricao FROM PalavrasChave WHERE descricao like '%{0}%'", p.Palavra);
            data = objDAL.DadosDataTable(sql);
            return data;
        }

        public DataTable listarIntencoesPalavra(PalavraDTO p)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("SELECT palavra, intencao, termos FROM PalavraIntencao as p inner join intencao as i on i.idintencao = p.intencao WHERE p.palavra = '{0}'", p.Idpalavra);
            data = objDAL.DadosDataTable(sql);
            return data;
        }

    }
}
