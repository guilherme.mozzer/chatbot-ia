﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using camadadedados;
using camadadeapresentacao;
using System.Data;

namespace camadadenegocios
{
    class RespostaBLL
    {
        dal objDAL = new dal();

        public DataTable inserir(RespostaDTO r)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("INSERT INTO Resposta (resposta) VALUES ('{0}'); select last_insert_id() as id;", r.Resposta);
            data = objDAL.DadosDataTable(sql);
            return data;
        }

        public void alterar(RespostaDTO r)
        {
            objDAL.conectado();
            string sql = String.Format("UPDATE Resposta set resposta = '{0}' where idresposta = {1}", r.Resposta, r.Idresposta);
            objDAL.ExecutarComandoSQL(sql);
        }

        public void excluir(RespostaDTO r)
        {
            objDAL.conectado();
            string sql = String.Format("DELETE FROM Resposta where idresposta = {0}", r.Idresposta);
            objDAL.ExecutarComandoSQL(sql);
        }

        public DataTable listarPalavraExiste(RespostaDTO r)
        {
            DataTable data = new DataTable();
            objDAL.conectado();
            string sql = String.Format("SELECT idresposta, resposta FROM Resposta WHERE resposta like '%{0}%'", r.Resposta);
            data = objDAL.DadosDataTable(sql);
            return data;
        }
    }
}
