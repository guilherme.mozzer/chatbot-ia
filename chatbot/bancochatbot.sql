CREATE TABLE IF NOT EXISTS Resposta (
  idresposta INT(11) NOT NULL auto_increment,
  resposta VARCHAR(512) NULL DEFAULT NULL,
   constraint pk_resposta PRIMARY KEY (idresposta))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS PalavrasChave (
  idpalavra INT(11) NOT NULL auto_increment,
  descricao VARCHAR(256) NULL DEFAULT NULL,
constraint pk_palavrachave PRIMARY KEY (idpalavra))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS Intencao (
  idintencao INT(11) NOT NULL auto_increment,
  termos VARCHAR(128) NULL DEFAULT NULL,
constraint pk_intencao PRIMARY KEY (idintencao))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS PalavraIntencao (
  palavra INT(11) NOT NULL,
  intencao INT(11) NOT NULL,
 constraint fk_palavra_chave_intencao FOREIGN KEY (palavra)
  REFERENCES  PalavrasChave (idpalavra),  
  constraint fk_intencao_palavra_chave FOREIGN KEY (intencao)
  REFERENCES Intencao (idintencao)
 )
 Engine = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS IntencaoResposta(
  intencao INT(11) NOT NULL,
  resposta INT(11) NOT NULL,
  constraint fk_intencao_resposta FOREIGN KEY (intencao)
  REFERENCES Intencao (idintencao),
  constraint fk_resposta_intencao FOREIGN KEY (resposta)
  REFERENCES Resposta (idresposta)
 )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;